# Google Colab

Course terkait: [Dasar Python](https://gitlab.com/marchgis/march-ed/2023/courses/dasar-python), [Implementasi Sistem Multimedia](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia)

## Instalasi Google Colab di Google Drive
1) Klik tombol **New**<br />
![Membuat resource baru di Google Drive](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-1-instalasi.png)

2) Klik **More**, lalu **Connect more apps**<br />
![Menu connect more apps di Google Drive](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-2-instalasi.png)

3) Pada **Google Workplace Marketplace**, cari **colab**<br />
![Cari Colab di Google Workplace Marketplace](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-3-instalasi.png)

4) Pada halaman **Google Colaboratory**, klik tombol **Install**<br />
![Instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-4-instalasi.png)

5)  **Get ready to install**, klik **continue**<br />
![Permission instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-5-instalasi.png)

6)  Pilih **Google Account** yang akan digunakan pada **Google Colaboratory**<br />
![Pilih akun instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-6-instalasi.png)

7)  Konfirmasi instalasi telah selesai<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-7-instalasi.png)

## Membuat File Notebook (IPYNB)

1) Masuk ke **Google Drive**, klik tombol **New**, lalu **More**, lalu **Google Colaboratory**<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-1-file-baru.png)
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/1-instalasi/1-8-instalasi.png)

2) Jika muncul dialog **Create in a shared folder ?**, klik tombol **Create and Share**<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-3-file-baru.png)

3) Klik dua kali pada judul file diatas untuk mengubah judul<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-4-file-baru.png)

4) Sel kode untuk menulis dan mengeksekusi kepingan kode Python / script lainnya<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-5-file-baru.png)

5) Tombil untuk mengakses outline dari file. Outline dapat dibuat dengan sel berformat Text<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-6-file-baru.png)

6) Tombol untuk mengakses folder<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-7-file-baru.png)

7) Coba tulis script dibawah ini, lalu jalankan dengan klik tombol eksekusi di kiri atas setiap sel<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-8-file-baru.png)

8) Tombol **+ Code** untuk membuat Sel Code baru<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-9-file-baru.png)

![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-10-file-baru.png)

9) Tombol **Text** digunakan untuk menambah Sel Text dengan format MD untuk membuat tulisan<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-11-file-baru.gif)

10) Tombol untuk menghapus Sel<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-12-file-baru.png)

12) Tombol untuk menukar posisi Sel<br />
![Konfirmasi selesai instalasi Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/2-file-baru/2-13-file-baru.png)

## Mengakses Folder

### Tab Folder
1) Untuk mengakses menu folder klik icon folder di sebelah kiri<br />
![Mengakses folder](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/3-folder/3-1-folder.png)

### Upload File
2) Tombol untuk mengupload file dari komputermu<br />
![Mengakses folder](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/3-folder/3-2-folder.png)

### Mount Folder dari Google Drive
3) Tombol untuk mengakses Google Drivemu<br />
![Mengakses folder](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/3-folder/3-3-folder.png)

4) Pertama kali klik tombol Google Drive, akan muncul dialog berikut, klik **Connect to Google Drive**<br />
![Mengakses folder](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/3-folder/3-4-folder.png)

5) Jika sudah terkoneksi, maka pada tab folder akan muncul folder **drive** yang merujuk ke folder di Google Drivemu<br />
![Mengakses folder](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/3-folder/3-5-folder.png)

6) Lihat, menyenangkan bukan, bisa mengoprek file Google Drivemu dengan **Python**<br />
![Mengakses folder](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/3-folder/3-6-folder.png)

### Mendapatkan Path / Alamat dari File 
7) Nah, setelah kita tahu cara mengakses folder, selanjutnya berikut cara untuk copy path / alamat dari file tersebut agar dapat kita akses via Python<br />
![Mengakses folder](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/3-folder/3-7-folder.gif)

## Mengakses Linux Command Line Interface

Tanda % digunakan diawal untuk mengakses CLI Linux 

1) Mengakses perintah **ls** untuk menampilkan direktori<br />
![Mengakses CLI di Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/4-cli/4-1-cli.png)

2) Mengakses perintah **pwd** untuk menampilkan direktori lokasi kita saat ini<br />
![Mengakses CLI di Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/4-cli/4-2-cli.png)

### Instalasi Package Python
3) Karena **pip** merupakan perintah di CLI Linux, maka untuk instalasi package Python dapat dilakukan dengan cara berikut, contoh instalasi package Requests untuk berinteraksi dengan URL web melalui HTTP Request<br />
![Mengakses CLI di Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/4-cli/4-3-cli.png)

4) Contoh instalasi package **OpenCV** untuk pemrosesan image<br />
![Mengakses CLI di Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/-/raw/main/4-cli/4-4-cli.png)

---
Course terkait: [Dasar Python](https://gitlab.com/marchgis/march-ed/2023/courses/dasar-python), [Implementasi Sistem Multimedia](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia)
